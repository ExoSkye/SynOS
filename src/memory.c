#include <memory.h>

static fdt_memory_res_block memoryBlocks[16];
static fdt_memory_res_block reservedMemoryBlocks[16];

fdt_memory_res_block* memory_get_blocks() {
    return &memoryBlocks[0];
}

fdt_memory_res_block* memory_get_reserved_blocks() {
    return &reservedMemoryBlocks[0];
}