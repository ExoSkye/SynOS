#include <dtb.h>
#include <endian.h>
#include <memory.h>
#include <stddef.h>
#include <utils.h>

void kernel_main(u64 dtb_ptr32, u64 x1, u64 x2, u64 x3) {
    fdt_header header = *((fdt_header*)dtb_ptr32);

    fdt_header_endian_fix(&header);

    if (header.magic != 0xd00dfeed) {
        emergency_halt();
    }

    if (header.last_comp_version != 16) {
        emergency_halt();
    }

    u64 cur_address = dtb_ptr32 + header.off_mem_rsvmap;

    for (u8 i = 0; i < 16; i++) {
        fdt_memory_res_block res_block = *((fdt_memory_res_block*)cur_address);
        fdt_memory_res_block_endian_fix(&res_block);

        memory_get_reserved_blocks()[i] = res_block;

        if (res_block.size == 0 && res_block.address == 0) {
            break;
        }

        cur_address += sizeof(fdt_memory_res_block);
    }
}