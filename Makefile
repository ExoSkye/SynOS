CLANG		 	:= clang
LLD				:= ld.lld
OBJCOPY			:= llvm-objcopy

BUILD_DIR = build
SRC_DIR = src

BD = $(BUILD_DIR)
SD = $(SRC_DIR)

INCLUDES := include third-party/packed
INCLUDE_FLAGS := $(addprefix -I,$(INCLUDES))

TARGET_TRIPLE 	?= aarch64-v8-none-elf

CLANG_OPS := -Wall -nostdlib -ffreestanding -mgeneral-regs-only $(INCLUDE_FLAGS) -mcpu=cortex-a72 -march=armv8-a --target=${TARGET_TRIPLE} -MMD -O0 -g
ASM_OPS := $(CLANG_OPS)
C_OPS := $(CLANG_OPS)


KERNEL_ASM_SOURCES := $(SD)/boot.S $(SD)/endian.S $(SD)/mm.S $(SD)/utils.S $(SD)/interrupts.S
KERNEL_C_SOURCES   := $(SD)/main.c $(SD)/dtb.c $(SD)/memory.c

KERNEL_ASM_OBJECTS := $(subst $(SD),$(BD),$(KERNEL_ASM_SOURCES:.S=_s.o))
KERNEL_C_OBJECTS   := $(subst $(SD),$(BD),$(KERNEL_C_SOURCES:.c=_c.o))

KERNEL_OBJECTS     := $(KERNEL_ASM_OBJECTS) $(KERNEL_C_OBJECTS) $(SD)/linker.ld

ifndef VERBOSE
    VERB := @
endif

.PHONY: all clean qemu qemu-debug
.SUFFIXES:

$(BD)/%_c.o: $(SD)/%.c
	$(VERB) echo Compiling $<
	$(VERB) mkdir -p $(@D)
	$(VERB) $(CLANG) $(C_OPS) -c $< -o $@

$(BD)/%_s.o: $(SD)/%.S
	$(VERB) echo Compiling $<
	$(VERB) mkdir -p $(@D)
	$(VERB) $(CLANG) $(ASM_OPS) -c $< -o $@

$(BD)/%.elf:
	$(VERB) echo Linking $@
	$(VERB) $(LLD) -o $@ $(filter %.o,$^) $(patsubst %,-T %,$(filter %.ld,$^))

$(BD)/kernel.elf: $(KERNEL_OBJECTS)

$(BD)/%.img: $(BD)/%.elf
	$(VERB) echo Creating $@
	$(VERB) $(OBJCOPY) $< -O binary $@

clean:
	$(VERB) rm -rf $(BD)

qemu: $(BD)/kernel.img
	$(VERB) echo Starting QEMU
	$(VERB) qemu-system-aarch64 -machine virt -machine virtualization=on -machine secure=on -cpu cortex-a72 -monitor stdio -display none -kernel $(BD)/kernel.img

qemu-debug: $(BD)/kernel.img
	$(VERB) echo Starting QEMU in debug mode
	$(VERB) qemu-system-aarch64 -machine virt -machine virtualization=on -machine secure=on -cpu cortex-a72 -monitor stdio -display none -kernel $(BD)/kernel.img -S -s

all: $(BD)/kernel.img
