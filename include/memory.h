#ifndef _MEMORY_H
#define _MEMORY_H

#include <dtb.h>

fdt_memory_res_block* memory_get_blocks();
fdt_memory_res_block* memory_get_reserved_blocks();

#endif //_MEMORY_H
